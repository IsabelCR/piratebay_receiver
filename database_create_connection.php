<?php

public class Connection{

  $servername = "localhost";
  $username = "root";
  $password = "12345";

  public function create_connection(){
    // Create connection
    $conn = new mysqli($this->servername, $this->username, $this->password);

    // Check connection
    if ($conn->connect_error) {
       die("Connection failed: " . $conn->connect_error);
    }
    echo "Connected successfully";
  }

  public function close_connection(){
    $conn->close();
  }

}
?>
