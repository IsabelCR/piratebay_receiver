<?php /*

 Basic usage

 for beginners: it is for command-line only execution. Webservers do not support threads.

 */
require_once("Thread.php");
require_once("ThreadQueue.php");
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;


// it is the function that will be called several times

function paralel( $name ) {
 echo "Iniciando";
  $f ='youtube-dl -o "/home/eliecer/Documentos/%(title)s.%(ext)s" '.$name;
    shell_exec($f);
    echo "FInalizado";
}

$connection = new AMQPConnection('localhost', 5672, 'ernarvaezm', '12345678');
$channel = $connection->channel();

$channel->queue_declare('videos_queue', false, false, false, false);

echo ' * Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg){
   $t1 = new Thread('paralel');
   $data = json_decode($msg->body, true);
   $t1->start($data['link']);
   $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
 };

$channel->basic_qos(null, 1, null);
$channel->basic_consume('videos_queue', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {

   $channel->wait();
}

$channel->close();
$connection->close();
