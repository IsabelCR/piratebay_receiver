<?php
// chdir(dirname(__DIR__));

require_once('vendor/autoload.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
// use WorkerReceiver;

$worker = new WorkerReceiver();

$worker->listen();


class WorkerReceiver
{
   public function listen()
   {
       $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
       $channel = $connection->channel();

       $channel->queue_declare(
           'video',            #queue
           false,              #passive
           false,              #durable, make sure that RabbitMQ will never lose our queue if a crash occurs
           false,              #exclusive - queues may only be accessed by the current connection
           false               #auto delete - the queue is deleted when all consumers have finished using it
           );

       /**
        * don't dispatch a new message to a worker until it has processed and
        * acknowledged the previous one. Instead, it will dispatch it to the
        * next worker that is not still busy.
        */
       $channel->basic_qos(
           null,      #prefetch size - prefetch window size in octets, null meaning "no specific limit"
           1,         #prefetch count - prefetch window in terms of whole messages
           null       #global - global=null to mean that the QoS settings should apply per-consumer, global=true to mean that the QoS settings should apply per-channel
           );

       /**
        * indicate interest in consuming messages from a particular queue. When they do
        * so, we say that they register a consumer or, simply put, subscribe to a queue.
        * Each consumer (subscription) has an identifier called a consumer tag
        */
       $channel->basic_consume(
           'video',                #queue
           '',                     #consumer tag - Identifier for the consumer, valid within the current channel. just string
           false,                  #no local - TRUE: the server will not send messages to the connection that published them
           false,                  #no ack, false - acks turned on, true - off.  send a proper acknowledgment from the worker, once we're done with a task
           false,                  #exclusive - queues may only be accessed by the current connection
           false,                  #no wait - TRUE: the server will not respond to the method. The client should not wait for a reply method
           array($this,'process') #callback
           );

       while(count($channel->callbacks)) {
           echo 'Waiting for incoming messages', "\n";
           $channel->wait();
       }

       $channel->close();
       $connection->close();
   }

   /**
    * process received request
    *
    * @param AMQPMessage $msg
    */
   public function process(AMQPMessage $msg)
   {
      echo "Downloading Video", "\n";
      $data = json_decode($msg->body, true);
      // echo $data['id'];
      $name = $data['link'];
      $download_video = "/home/estudiante/Escritorio/pirate_bay/Videos/".$data['id'].".mp4";
      $string = ('youtube-dl ' . escapeshellarg($name) . ' -f 18 -o ' .escapeshellarg($download_video));
      $descriptorspec = array(
                                0 => array("pipe", "r"), // stdin
                                1 => array("pipe", "w"), // stdout
                                2 => array("pipe", "w"), // stderr
                                );
      $process = proc_open($string, $descriptorspec, $pipes);
      $stdout = stream_get_contents($pipes[1]);
      fclose($pipes[1]);
      $rep = proc_close($process);
      if ($rep == 0) {
        shell_exec($download_video);
        $this->update_video_table($data['id'], 'Downloaded');
        echo "Video Downloaded", "\n";
      }else {
        $this->update_video_table($data['id'], 'Fail');
      }
       /**
        * If a consumer dies without sending an acknowledgement the AMQP broker
        * will redeliver it to another consumer or, if none are available at the
        * time, the broker will wait until at least one consumer is registered
        * for the same queue before attempting redelivery
        */
      $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
   }

   public function update_video_table($video_id, $message){
     $servername = "localhost";
     $username = "root";
     $password = "12345";
     $dbname = "mysql";

     // Create connection
     $conn = new mysqli($servername, $username, $password, $dbname);

     // Check connection
     if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
     }
     echo "Connected successfully", "\n";

     $sql = "UPDATE videos SET status= '".$message."' WHERE id=".$video_id;
    if ($conn->query($sql) === TRUE) {
        echo "Database created successfully", "\n";
    } else {
        echo "Error creating database: " . $conn->error;
    }

    $conn->close();
   }

}
